<?php
session_start();

if (empty($_SESSION['lang'])) {
    define('LANG', 'en');
} else {
    define('LANG', $_SESSION['lang']);
}

$yii=dirname(__FILE__).'/../../../framework/yiilite.php';
require dirname(__FILE__).'/../../admin/protected/config/config.php';
$config=dirname(__FILE__).'/../../admin/protected/config/de_DE/landing_dealer/index-site.php';
require dirname(__FILE__).'/../../admin/protected/vendor/autoload.php';

require_once($yii);
Yii::createWebApplication($config)->run();
