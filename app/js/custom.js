/* --------------------------------------------

Page Loader

-------------------------------------------- */

$(window).load(function() {

	'use strict';

	$(".loader-item").delay(1700).fadeOut();

	$("#pageloader").delay(1800).fadeOut("slow");
	
});	   


$(function() {


/* --------------------------------------------

Carousel-slider

-------------------------------------------- */	
	
	'use strict';
	
	$(".owl-demo").owlCarousel({

		items : 1,
		itemsDesktop : [1199, 1],
		itemsDesktopSmall : [991, 1],
		itemsTablet : [768, 1],
		itemsTabletSmall : false,
		itemsMobile : [479, 1],

		lazyLoad : true,

		autoPlay: false,

		navigation : false

	});
	
	
	
/* --------------------------------------------

Text Slider Home Page

-------------------------------------------- */

	  'use strict';



      $('.text-slider').flexslider({

        animation: "slide",

		selector: ".slide-text li",

		controlNav: false,

		directionNav: false,

		slideshowSpeed: 10000,

		touch: true,

		useCSS: false,

		direction: "vertical",

        before: function(slider){        

		 //var height = $('.text-slider').find('.flex-viewport').innerHeight();

		 //$('.text-slider').find('li').css({ height: 200 + 'px' });

        }

      });


});


/* --------------------------------------------

Home Background Super Slider 

-------------------------------------------- */

$('#slides').superslides({

	animation: 'fade',

});




$(function($) {	

	'use strict';	

	/* --------------------------------------------

		Animated Items

	-------------------------------------------- */



	$('.animated').appear(function() {

		var elem = $(this);

		var animation = elem.data('animation');

		if ( !elem.hasClass('visible') ) {

			var animationDelay = elem.data('animation-delay');

			if ( animationDelay ) {



				setTimeout(function(){

					elem.addClass( animation + " visible" );

				}, animationDelay);



			} else {

				elem.addClass( animation + " visible" );

			}

		}

	});

	

	/* --------------------------------------------

		Load More 

	-------------------------------------------- */

	var loadtext = $('.load-more');

	$(".load-posts").click(function() {

		if($(this).hasClass('disable')) return false;

		

			$(this).html('<i class="fa fa-spin fa-spinner"></i>');

			

			var $hidden = loadtext.filter(':hidden:first').delay(600);  

   

		   	if (!$hidden.next('.load-more').length) {

			   $hidden.fadeIn(500);

				$(this).addClass('disable');

				$(this).fadeTo("slow", 0.23)/*.delay(600)*/

				.queue(function(n) {

				 $(this).remove();

				 n();

				}).fadeTo("slow", 1);

			

		   	} else {

				$hidden.fadeIn(500);

				$(this).fadeTo("slow", 0.23)/*.delay(600)*/

				.queue(function(g) {

				 $(this).html('Показать еще<i class="flaticon-arrow209">');

				 g();

				}).fadeTo("slow", 1);			

		   	}

	});

	

});






/* --------------------------------------------

 Scroll Navigation

-------------------------------------------- */	

$(function() {

	'use strict';	

	jQuery('.scroll').bind('click', function(event) {

		var $anchor = jQuery(this);

		var headerH = jQuery('#navigation').outerHeight();

			jQuery('html, body').stop().animate({					

				scrollTop : jQuery($anchor.attr('href')).offset().top  + 2 + "px"

			}, 1200, 'easeInOutExpo');



		event.preventDefault();

	});

});

/* --------------------------------------------

 Active Navigation

-------------------------------------------- */



	jQuery('body').scrollspy({ 

		target: '#topnav',

		offset: 95

	})

/* --------------------------------------------

 Video Script

-------------------------------------------- */


$(".player").mb_YTPlayer();	


			



$(function() { 


	// Menus hide after click --  mobile devices

	'use strict';

	$('.nav li a').click(function () {

		 $('.navbar-collapse').removeClass('in');

	});


});	   

	$(".lightbox").fancybox({
		padding: 1
	});



/* -------------------------------------------- 

 Blog Flex Slider

-------------------------------------------- */

$('.flexslider').flexslider({

	animation: 'fade',

	slideshow: false,

	animationLoop: false,

	controlNav: true,
	directionNav: true

});

