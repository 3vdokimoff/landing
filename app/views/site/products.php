<section id="products" class="how-it-works">

    <div class=" how-it-works-inner black">
        <p>&nbsp;</p>
        <!-- Title & Desc Row Begins -->
        <div class="row">
            <div class="col-md-12 header text-center">
                <!-- Title -->
                <div class="title">
                    <br>
                    <h2><?= $dealer_lang['newGeneration'][$lang] ?></h2>
                </div>
                <div class="col-md-8 col-md-offset-3 vde18">
                    <div class="text-left">
                        <!-- Desc -->
                        <p><?= $dealer_lang['text2'][$lang] ?> </p>

                        <p>&nbsp;</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-2 header text-center">
                <!-- Title -->
                <div class="title1 text-center">
                    <h2><STRONG>Laitovo </STRONG><?= $dealer_lang['offers'][$lang] ?>  </h2>
                </div>
            </div>
        </div>


        <div class="row">

            <!-- Left Part Begins -->

            <!-- Left Part Ends -->
            <div class="col-md-5 text-center">
                <div class="">
                </div>
            </div>

            <div class="col-md-7">
                <div class="text-left animated" data-animation="fadeInLeft" data-animation-delay="500">
                    <!-- Desc -->
                    <h3>Laitovo
                        <small>premium</small>
                    </h3>
                </div>
            </div>

        </div>
    </div>

    <div class="container how-it-works-inner black animated" data-animation="fadeInLeft" data-animation-delay="500">

        <div class="row">

            <!-- Left Part Begins -->

            <!-- Left Part Ends -->
            <div class="col-md-5 text-center">
                <div class="">
                    <img class="h-computer"
                         src="https://laitovo.eu/upload/images/Photo/original/230e182e14c88abd26bb5f0f42f86a62_1.jpg">
                </div>
            </div>

            <div class="col-md-7">
                <div class="text-left">
                    <!-- Desc -->

                    <p><?= $dealer_lang['premium'][$lang] ?></p>

                    <p>&nbsp;</p>
                </div>
            </div>
            <div class="col-md-12">
                <div class="text-left">
                    <div class="row 150%">
                        <div class="col-md-3">
                            <div class="image fit captioned">
                                <img src="https://laitovo.eu/upload/images/Cars/thumbnail/021733f603668c2f856d03a430611420.jpeg">
                                <h4 class="catcar"><?= $dealer_lang['mediumCar'][$lang] ?></h4>
                            </div>
                        </div>
                        <div class="col-md-9">

                            <div class="col-md-3">
                                <div class="image fit captioned text-center">
                                    <img class="zoom_img"
                                         data-zoom-image="https://laitovo.eu/upload/images/Photo/original/79a82aa2438c48dff945115e42feaa5a_1.jpg"
                                         src="https://laitovo.eu/upload/images/Photo/thumbnail/79a82aa2438c48dff945115e42feaa5a_1.jpg"
                                         alt=""/>
                                    <h6><?= $dealer_lang['FD'][$lang] ?></h6>
                                    <h4><?= $dealer_lang['retailPrice'][$lang] ?><s> <br><?= 83 ?></s> &#8364;<br>
                                        <span style="color: red;"><?= $dealer_lang['salePrice'][$lang] ?> <br>42 &#8364; - 58 &#8364;<? //=round(2999-2999*10/100)?>
                                            </span>
                                    </h4>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="image fit captioned text-center">
                                    <img class="zoom_img"
                                         data-zoom-image="https://laitovo.eu/upload/images/Photo/original/b7342562c0eebf7b19feeb783b0769cc.jpg"
                                         src="https://laitovo.eu/upload/images/Photo/thumbnail/b7342562c0eebf7b19feeb783b0769cc.jpg"
                                         alt=""/>
                                    <h6><?= $dealer_lang['RD'][$lang] ?></h6>
                                    <h4><?= $dealer_lang['retailPrice'][$lang] ?> <s><br><?= 83 ?></s> &#8364;<br>
                                        <span style="color: red;"><?= $dealer_lang['salePrice'][$lang] ?> <br>42 &#8364; - 58 &#8364;<? //=round(2999-2999*10/100)?>
                                            </span>
                                    </h4>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="image fit captioned text-center">
                                    <img class="zoom_img1"
                                         data-zoom-image="https://laitovo.eu/upload/images/Catalog/original/c36bbd258b7ee694eb987221b2b197b0_1_1.jpg"
                                         src="https://laitovo.eu/upload/images/Catalog/thumbnail/c36bbd258b7ee694eb987221b2b197b0_1_1.jpg"
                                         alt=""/>
                                    <h6><?= $dealer_lang['rearHemisphere'][$lang] ?></h6>
                                    <h4><?= $dealer_lang['retailPrice'][$lang] ?> <s><br><?= 132 ?></s> &#8364;<br>
                                        <span style="color: red;"><?= $dealer_lang['salePrice'][$lang] ?><br> 66 &#8364; - 92 &#8364;<? //=round(2999-2999*10/100)?>
                                            </span>
                                    </h4>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="image fit captioned text-center">
                                    <img class="zoom_img1"
                                         data-zoom-image="https://laitovo.eu/upload/images/Catalog/original/cd7846bdb055e819c62d60b22c9e003a.jpg"
                                         src="https://laitovo.eu/upload/images/Catalog/thumbnail/cd7846bdb055e819c62d60b22c9e003a.jpg"
                                         alt=""/>
                                    <h6><?= $dealer_lang['completeSet'][$lang] ?></h6>
                                    <h4><?= $dealer_lang['retailPrice'][$lang] ?> <s><br><?= 215 ?></s> &#8364;<br>
                                        <span style="color: red;"><?= $dealer_lang['salePrice'][$lang] ?> <br> 108 &#8364; - 151 &#8364;<? //=round(2999-2999*10/100)?>
                                            </span>
                                    </h4>
                                    <small class="zvezdochkabezfort">* <?= $dealer_lang['withoutVents'][$lang] ?></small>
                                </div>
                            </div>
                            <small class="pull-right">* <?= $dealer_lang['withoutVents'][$lang] ?></small>
                        </div>
                    </div>
                    <hr>

                    <div class="row 150%">
                        <div class="col-md-3">
                            <div class="image fit captioned">
                                <img src="https://laitovo.eu/upload/images/Cars/thumbnail/dcae60ef21367803a6a4a42628b6239b.jpg">
                                <h4 class="catcar"><?= $dealer_lang['offRoad'][$lang] ?></h4>
                            </div>
                        </div>
                        <div class="col-md-9">

                            <div class="col-md-3">
                                <div class="image fit captioned text-center">
                                    <img class="zoom_img"
                                         data-zoom-image="https://laitovo.eu/upload/images/Photo/original/79a82aa2438c48dff945115e42feaa5a_1.jpg"
                                         src="https://laitovo.eu/upload/images/Photo/thumbnail/79a82aa2438c48dff945115e42feaa5a_1.jpg"
                                         alt=""/>
                                    <h6><?= $dealer_lang['FD'][$lang] ?></h6>
                                    <h4><?= $dealer_lang['retailPrice'][$lang] ?> <s><br><?= 100 ?></s> &#8364;<br>
                                        <span style="color: red;"><?= $dealer_lang['salePrice'][$lang] ?> <br>50 &#8364; - 70 &#8364;<? //=round(2999-2999*10/100)?>
                                            </span>
                                    </h4>
                                    <!-- <a href="#" class="button big btn btn-default orderscrolbtn2">Заказать</a><input type="hidden" value="Комплект передних боковых экранов Laitovo"> -->
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="image fit captioned text-center">
                                    <img class="zoom_img"
                                         data-zoom-image="https://laitovo.eu/upload/images/Photo/original/b7342562c0eebf7b19feeb783b0769cc.jpg"
                                         src="https://laitovo.eu/upload/images/Photo/thumbnail/b7342562c0eebf7b19feeb783b0769cc.jpg"
                                         alt=""/>
                                    <h6><?= $dealer_lang['RD'][$lang] ?></h6>
                                    <h4><?= $dealer_lang['retailPrice'][$lang] ?> <s><br><?= 100 ?></s> &#8364;<br>
                                        <span style="color: red;"><?= $dealer_lang['salePrice'][$lang] ?> <br>50 &#8364; - 70 &#8364;<? //=round(2999-2999*10/100)?>
                                            </span>
                                    </h4>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="image fit captioned text-center">
                                    <img class="zoom_img1"
                                         data-zoom-image="https://laitovo.eu/upload/images/Catalog/original/c36bbd258b7ee694eb987221b2b197b0_1_1.jpg"
                                         src="https://laitovo.eu/upload/images/Catalog/thumbnail/c36bbd258b7ee694eb987221b2b197b0_1_1.jpg"
                                         alt=""/>
                                    <h6><?= $dealer_lang['rearHemisphere'][$lang] ?></h6>
                                    <h4><?= $dealer_lang['retailPrice'][$lang] ?> <s><br><?= 166 ?></s> &#8364;<br>
                                        <span style="color: red;"><?= $dealer_lang['salePrice'][$lang] ?> <br>83 &#8364; - 116 &#8364;<? //=round(2999-2999*10/100)?>
                                           </span>
                                    </h4>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="image fit captioned text-center">
                                    <img class="zoom_img1"
                                         data-zoom-image="https://laitovo.eu/upload/images/Catalog/original/cd7846bdb055e819c62d60b22c9e003a.jpg"
                                         src="https://laitovo.eu/upload/images/Catalog/thumbnail/cd7846bdb055e819c62d60b22c9e003a.jpg"
                                         alt=""/>
                                    <h6><?= $dealer_lang['completeSet'][$lang] ?></h6>
                                    <h4><?= $dealer_lang['retailPrice'][$lang] ?> <s><br><?= 266 ?></s> &#8364;<br>
                                        <span style="color: red;"><?= $dealer_lang['salePrice'][$lang] ?> <br>133 &#8364; - 186 &#8364;<? //=round(2999-2999*10/100)?>
                                            </span>
                                    </h4>
                                    <small class="zvezdochkabezfort">* <?= $dealer_lang['withoutVents'][$lang] ?></small>
                                </div>
                            </div>
                            <small class="pull-right">* <?= $dealer_lang['withoutVents'][$lang] ?></small>
                        </div>
                    </div>

                    <p>&nbsp;</p>
                    <p>&nbsp;</p>

                </div>
            </div>

        </div>
    </div>

    <div class=" how-it-works-inner black">

        <div class="row">

            <div class="col-md-5 text-center">
                <img class="h-computer"
                     src="https://laitovo.eu/upload/images/Photo/original/d84a0d63d1793f1131eb619e240b1fdc.jpg">
            </div>

            <div class="col-md-7">
                <div class="text-left animated" data-animation="fadeInLeft" data-animation-delay="500">
                    <h3 class="left"><?= $dealer_lang['chikoEconomy'][$lang] ?></h3>
                </div>
            </div>


        </div>
    </div>

    <div class="container how-it-works-inner black animated" data-animation="fadeInRight" data-animation-delay="500">

        <div class="row">


            <div class="col-md-5 text-center">
            </div>

            <div class="col-md-7">
                <div class="text-left">
                    <!-- Desc -->

                    <p><?= $dealer_lang['chikoDesc'][$lang] ?>. </p>
                    <p>&nbsp;</p>


                    <div class="row 150%">
                        <div class="col-md-12">

                            <div class="col-md-3">
                                <div class="image fit captioned text-center">
                                    <img class="zoom_img"
                                         data-zoom-image="https://laitovo.eu/upload/images/Gallery/1806c657b1f4adf141be6f873b847afb.jpg"
                                         src="https://laitovo.eu/upload/images/Gallery/1806c657b1f4adf141be6f873b847afb.jpg"
                                         alt=""/>
                                    <h6><?= $dealer_lang['FD'][$lang] ?></h6>
                                    <h4><?= $dealer_lang['retailPrice'][$lang] ?><br> <s><?= 59 ?> &#8364;</s><br>
                                        <span style="color: red;"><?= $dealer_lang['salePrice'][$lang] ?><br>  35 &#8364;<? //=round(2999-2999*10/100)?>
                                            </span>
                                    </h4>
                                    <!-- <a href="#" class="button big btn btn-default orderscrolbtn2">Заказать</a><input type="hidden" value="Комплект передних боковых экранов Chiko"> -->
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="image fit captioned text-center">
                                    <img class="zoom_img"
                                         data-zoom-image="https://laitovo.eu/upload/images/Gallery/2052dc09a2fb92e83dac143d8e919d95.jpg"
                                         src="https://laitovo.eu/upload/images/Gallery/2052dc09a2fb92e83dac143d8e919d95.jpg"
                                         alt=""/>
                                    <h6><?= $dealer_lang['RD'][$lang] ?></h6>
                                    <h4><?= $dealer_lang['retailPrice'][$lang] ?><br><s><?= 59 ?> &#8364;</s><br>
                                        <span style="color: red;"><?= $dealer_lang['salePrice'][$lang] ?> <br> 35 &#8364;<? //=round(2999-2999*10/100)?>
                                            </span>
                                    </h4>
                                    <!-- <a href="#" class="button big btn btn-default orderscrolbtn2">Заказать</a><input type="hidden" value="Комплект задних боковых экранов Chiko"> -->
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="image fit captioned text-center">
                                    <img class="zoom_img1"
                                         data-zoom-image="https://laitovo.eu/upload/images/Catalog/58a46426b953f903ef4b8b817e24536d.jpg"
                                         src="https://laitovo.eu/upload/images/Catalog/thumbnail/58a46426b953f903ef4b8b817e24536d.jpg"
                                         alt=""/>
                                    <h6><?= $dealer_lang['rearHemisphere'][$lang] ?></h6>
                                    <h4><?= $dealer_lang['retailPrice'][$lang] ?><br><s><?= 108 ?> &#8364;</s><br>
                                        <span style="color: red;"><?= $dealer_lang['salePrice'][$lang] ?><br> 65 &#8364;<? //=round(2999-2999*10/100)?>
                                            </span>
                                    </h4>
                                    <small class="zvezdochkabezfort">* <?= $dealer_lang['withoutVents'][$lang] ?></small>
                                    <!-- <a href="#" class="button big btn btn-default orderscrolbtn2">Заказать</a><input type="hidden" value="Комплект экранов Chiko на окна заднй полусферы автомобиля"> -->
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="image fit captioned text-center">
                                    <img class="zoom_img1"
                                         data-zoom-image="https://laitovo.eu/upload/images/Catalog/3395cc1d3fe4c70feb898e883aeceb49.jpg"
                                         src="https://laitovo.eu/upload/images/Catalog/thumbnail/3395cc1d3fe4c70feb898e883aeceb49.jpg"
                                         alt=""/>
                                    <h6><?= $dealer_lang['completeSet'][$lang] ?></h6>
                                    <h4><?= $dealer_lang['retailPrice'][$lang] ?><br><s><?= 156 ?> &#8364;</s><br>
                                        <span style="color: red;"><?= $dealer_lang['salePrice'][$lang] ?><br>  94 &#8364;<? //=round(2999-2999*10/100)?>
                                            </span>
                                    </h4>
                                    <small class="zvezdochkabezfort">* <?= $dealer_lang['withoutVents'][$lang] ?></small>
                                </div>
                            </div>
                            <small class="pull-right">* <?= $dealer_lang['withoutVents'][$lang] ?></small>
                        </div>
                    </div>
                    <!-- Lists -->
                </div>
            </div>
        </div>
    </div>

</section>