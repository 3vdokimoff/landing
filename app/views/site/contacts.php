<section id="zayavka" class="screenshots">
    <div class="container screenshots-inner" data-animated="bounceIn">

        <div class="title">
            <h2><?= $dealer_lang['signUp'][$lang] ?></h2>
        </div>
    </div>
</section>
<section id="screenshots" class="contact">
    <div class="container screenshots-inner" data-animated="bounceIn">

        <p class="desc text-center" style="color: #000;"><?= $dealer_lang['fillForm'][$lang] ?></p>

        <div class="row">
            <div class="col-md-8 col-md-offset-2 animated" data-animation="fadeInUp" data-animation-delay="300">
                <p class="form-message" style="display: none;"></p>
                <a name="orderform"></a>
                <div class="contact-form">
                    <!-- Form Begins -->
                    <form role="form" name="contactform" class="form-horizontal" id="contactform" method="post"
                          action=<?= Yii::app()->createUrl('cooperation/cooperation/create') ?>>
                        <div class="row">
                            <div class="col-md-6">
                                <!-- Field 1 -->
                                <div class="input-text form-group">
                                    <input type="text" name="contact_name" id="contact_name"
                                           class="input-name form-control" placeholder="<?= $dealer_lang['name'][$lang] ?>"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <!-- Field 2 -->
                                <div class="input-email form-group">
                                    <input type="email" name="contact_email" id="contact_email"
                                           class="input-email form-control" placeholder="E-mail"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <!-- Field 1 -->
                                <div class="input-text form-group">
                                    <input type="text" name="contact_phone" id="contact_phone"
                                            class="input-phone form-control"
                                           placeholder="<?= $dealer_lang['telephone'][$lang] ?>"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-text form-group">
                                    <?php
                                    Yii::import('location.models.Location');
                                    Yii::import('application.helpers.ArrayHelper');
                                    $criteria = new CDbCriteria();
                                    $criteria->condition = 'status=1 AND pid = 0';
                                    $criteria->order = 'name';
                                    $region = Location::model()->findAll($criteria);
                                    $arrRegion = ArrayHelper::map($region, 'id', 'name');
                                    $arrRegion[''] = 'Select country';
                                    ?>
                                    <?= CHtml::dropDownList('region', '', $arrRegion, ['class'=>'select-search selectregions', 'style' => 'width: 100%;', 'id' => 'region', 'required' => 'required' ]) ?>
                                    <style>
                                        .select2-container .select2-selection--single {
                                            height: 60px;
                                        }
                                        .select2-container--default .select2-selection--single .select2-selection__rendered {
                                            line-height: 60px;
                                            color: #000;
                                            background-color: #c1c6cc;
                                        }
                                        .select2-results__option {
                                            color: #000;
                                        }
                                    </style>
                                </div>
                            </div>
                        </div>
                        <button style="background:#ffa700;" class="btn btn-default" type="submit">Send</button>
                    </form>
                    <!-- Form Ends -->
                </div>
            </div>
        </div>

    </div>
</section>
<!-- Contact Section Ends -->
<!-- Download Now Section Begins -->
<section id="contactsection" class="download-now">
    <div class="container download-now-inner black">
        <!-- Download Buttons -->
        <div class="row text-center col-sm-12 vde18">

            <div class="title">
                <h2><?= $dealer_lang['contacts'][$lang] ?></h2>
            </div>

            <p> <?= $dealer_lang['telephone'][$lang] ?>: +49 (0)40-87407233 <br>
                Email: partner@laitovo.de <br>
                <a rel="nofollow" target="_blank" href="https://www.laitovo.ru"><b>www.laitovo.eu</b></a>
            </p>

        </div>
    </div>
</section>
<section id="patent" class="how-it-works ">

    <div class="container how-it-works-inner black">
        <!-- Title & Desc Row Begins -->
        <div class="row">
            <div class="col-md-12 header text-center">
                <!-- Title -->
                <div class="title">
                    <h2><?= $dealer_lang['bewareImitations'][$lang] ?></h2>
                </div>
                <!-- Description -->
            </div>
        </div>
    </div>
    <div class=" how-it-works-inner black animated" data-animation="fadeInRight" data-animation-delay="500">
        <div class="row">
            <div class="col-md-8">

                <h3 class="right"><?= $dealer_lang['patented'][$lang] ?></h3>

            </div>
            <div class="col-md-4 text-center">
            </div>

            <!-- Right Part Ends -->
        </div>
        <!-- How It Works Row Ends -->
    </div>

    <div class="container how-it-works-inner black">
        <div class="row">
            <div class="col-md-9">
                <div class=" text-left animated" data-animation="fadeInRight" data-animation-delay="500">
                    <?= $dealer_lang['brandLaitovo'][$lang] ?>
                    <?= $dealer_lang['inventor'][$lang] ?>
                    <?= $dealer_lang['soleRights'][$lang] ?>
                    <?= $dealer_lang['dealerLicense'][$lang] ?>
                </div>
            </div>
            <div class="col-md-3 text-right">
                <div class="benefits">
                    <img src="https://laitovo.eu/de_DE/upload/images/Trademark%20principal%20register-1%281%29.jpg"
                         alt="" class="phone-image-right relative animated" data-animation="fadeInLeft"
                         data-animation-delay="1200">
                </div>
            </div>

            <!-- Right Part Ends -->
        </div>
        <!-- How It Works Row Ends -->
    </div>
    <!-- Container Ends -->
</section>