<?php
if (isset($_SESSION['lang']) && ($_SESSION['lang'] == 'de' || $_SESSION['lang'] == 'en')) {
    $lang = $_SESSION['lang'];
} else {
    $lang = 'en';
}

$lang_search = $lang == 'en' ? 'de' : 'en';

if (isset($_GET['lang'])) {
    if ($_GET['lang'] == 'de' || $_GET['lang'] == 'en') {
        $_SESSION['lang'] = $_GET['lang'];
    }
    header("Location: /");
}
$dealer_lang = require __DIR__ . '/lang.php';

?>
<style type="text/css">
    .language {
        margin-right: 20px;
        cursor: pointer;
    }

    .language > span {
        color: #fff;
    }
    
    .language > ul {
        display: none;
    }

    .nav-tabs > li > a {
        border-color: #eee #eee #ddd;
    }

    .searchbtn, .searchbtn:hover {
        background: url("<?=Yii::app()->theme->baseUrl?>/images/interface/filter.png") transparent no-repeat 10px;;
        display: inline-block;
        vertical-align: middle;
        font: 0/0 a;
        color: transparent;
        text-shadow: none;
        border: 0;
        padding: 7px 16px 8px;
        position: absolute;
        width: 2rem !important;
        height: 2rem;
        margin: -1rem 0 0;
        right: 1rem;
        top: 50%;
    }

    .h-computer .searchbtn, .h-computer .searchbtn:hover {
        height: 6.2rem;
    }

    .formcircleform {
        padding: 9px;
    }

    .formcircle {
        border-radius: 500px;

    }
    .markauto .grid .block:nth-of-type(6n+1) {
        clear: left;
    }

    .markauto .model .block:nth-of-type(3n+1) {
        clear: left;
    }

    .markauto .goods .block:nth-of-type(4n+1) {
        clear: left;
    }



    .modal-backdrop {
        z-index: 3 !important;
    }

    .zvezdochkabezfort {
        display: none;
    }

    .mybyform_cartochka .zvezdochkabezfort {
        display: block;
    }
</style>
<!-- Page Loader -->
<!-- Header Begins -->
<!-- Background Slider Begins -->
<?php require_once 'header.php'?>
<!-- Welcome Section Begins -->
<?php require_once 'welcome.php'?>
<!-- Welcome Section Ends -->
<?php require_once 'products.php'?>
<!--    Вставки романа-->
<?php require_once 'partner.php'?>
<!-- How It Works Ends -->
<!-- Additional features Section Begins -->
<?php require_once 'additional_section.php'?>
<?php require_once 'contacts.php'?>
<?php require_once 'footer.php'?>
<?php require_once 'js.php'?>