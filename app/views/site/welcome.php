<section id="welcome" class="welcome-section " style="">

    <div class="welcome-inner container" style="">
        <!-- Title & Desc Row Begins -->
        <div class="row">
            <div class="col-md-6 header text-left">
                <!-- Title -->
                <div class="title">
                    <h2 class="white"><span><?= $dealer_lang['brand'][$lang] ?></span></h2>
                    <!-- <p>&nbsp;</p> -->
                </div>
                <!-- Description -->
                <p class="white animated" data-animation="fadeInRight" data-animation-delay="500"
                   style="text-align:justify;">
                    <img class="col-xs-3" src="https://laitovo.eu/media/upload/files/germany.gif?ts=1496233395610">
                    <?= $dealer_lang['text1'][$lang] ?> <a
                        rel="nofollow" target="_blank"
                        href="https://laitovo.eu/de_DE/upload/files/Handelsregisterauszug_%20Laitovo-GmbH.pdf"><b>Laitovo
                            Manufactory Trading & Services GmbH</b></a> <?= $dealer_lang['text1-1'][$lang] ?> <a rel="nofollow"
                                                                                           target="_blank"
                                                                                           href="https://goo.gl/maps/K5fGFvCLae82">Hamburg.</a><?= $dealer_lang['text1-2'][$lang] ?> </p>
            </div>
        </div>
        <!-- Title & Desc Row Ends -->
    </div>

</section>