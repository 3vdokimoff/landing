<?php
return [
    'sale' => [
        'de' => 'Auto Sonnenschutz Grosshandelsverkauf',
        'en' => 'Wholesale',
    ],
    'menu' => [
        'de' => 'Menü',
        'en' => 'Menu',
    ],
    'prodPrice' => [
        'de' => 'Produktion und Preise',
        'en' => 'Products and prices',
    ],
    'terms' => [
        'de' => 'Bedingungen der Zusammenarbeit',
        'en' => 'Business terms and conditions',
    ],
    'getDealerStatus' => [
        'de' => 'Bewerben Sie sich zur Partnerschaft',
        'en' => 'Get the Dealer Status',
    ],
    'contacts' => [
        'de' => 'Kontakt',
        'en' => 'Contacts',
    ],
    'callBack' => [
        'de' => 'Rückruf anfordern',
        'en' => 'Request a callback',
    ],
        'Zayavkaobslujivanie' => [
        'de' => 'PARTNERSCHAFT ANMELDEN',
        'en' => 'REQUEST FOR PARTNESHIP',
    ],
    //Альтернатива тонировке
    'slogan1' => [
        'de' => 'Alternative zu Scheibentönung',
        'en' => 'Alternative to vehicle tinting',
    ],
    //Все преимущества тонировки с открытым окном
    'slogan2' => [
        'de' => 'Alle Vorteile  der Scheibentönung mit geöffnetem Fenster',
        'en' => 'Competitive advantages of vehicle tinting with opened window',
    ],
    //ЗАРАБАТЫВАТЬ С LAITOVO ЛЕГКО
    'slogan3' => [
        'de' => 'Verdienen Sie mit Laitovo',
        'en' => 'Earn money with Laitovo',
    ],
    //ГИБКАЯ СИСТЕМА СКИДОК ДЛЯ ЛЮБОГО БИЗНЕСА
    'slogan4' => [
        'de' => 'Flexibles Rabattsystem für Business jeder Grösse',
        'en' => 'Flexible discount system for Business of any scale',
    ],
    //АВТОШТОРКИ LAITOVO ОПТОМ
    'slogan5' => [
        'de' => 'Auto-Sonnenchutz im Grosshandel  kaufen',
        'en' => 'Buy car sunshades wholesale',
    ],
    'brand' => [
        'de' => 'Laitovo - Internationale Handelsmarke',
        'en' => 'Laitovo - International Brand',
    ],
    //Защитные экраны Laitovo производятся под немецким брендом ...
    'text1' => [
        'de' => 'Schutzschirme Laitovo  werden unter eigener deutscher  Brandmarke hergestellt. 
Der Stammsitz  von ',
        'en' => 'Protective screens  for automobile windows are produced under  own german trademark. Head office of 
',
    ],
    'text1-1' => [
        'de' => ' befindet sich in',
        'en' => ' is located in
',
    ],
    'text1-2' => [
        'de' => ' Derzeit macht Laitovo Umsätze in der Europäischen Union, im Nahen Osten, in Zentral- und Ostasien.',
        'en' => ' ',
    ],
    'newGeneration' => [
        'de' => 'Laitovo - Sonneschutz der neuen Generation!',
        'en' => 'Laitovo car tinting of the new generation!',
    ],
    //Преимуществом данного вида тонирования является простота её снятия и установки. Благодаря запатентованной технологии креплений, Laitovo устанавливаются без клея, сверления и без вмешательства в конструкцию автомобиля.
    'text2' => [
        'de' => 'Vorteil von dieser Art des Sonnenschutzes im Auto besteht in der gebrauchfreundlichen Abbau- und Einbau Vorgehensweise. Dank patentgeschütztem Verfahren der Befestigung werden Laitovo Schutzschirme ohne Leim, Bohren  und ohne Veränderung von KFZ-konstruktion angebracht',
        'en' => 'Competitive advantages of vehicle tinting lie in ease of operation  of removal and installation. Due to proprietary technology of clip holders Laitovo tinting is installed without glue, drilling and making any alterations in automobile design.
',
    ],
    'offers' => [
        'de' => 'bietet die  günstigen Bedingungen der Zusammenarbeit  in den Preissegmenten  von <STRONG> Premium und Econom-Klasse an </STRONG> ',
        'en' => 'offers favourable terms  of partnership in tiers of prices:<STRONG> Premium and Economy </STRONG>',
    ],
    'premium' => [
        'de' => '<b> &nbsp;&nbsp; Laitovo Sonnenschutz </b> ist ein teures Produkt für einen Käufer, der nur das beste Zubehör für sein Auto auswählt.</br>  &nbsp;&nbsp; Bei einem Laitovo Sonnenschutzschirm handelt es sich um ein festes Metallgehäuse, das mit verschleißfestem, elastischem kleinmaschigem High-Tech-Gewebe beschlagen wird. Das Gewebe verhindert das Eindringen von schädlicher direkter Sonneneinstrahlung ins Auto und schützt damit Fahrer und Fahrgästen. </br>  &nbsp;&nbsp; Schutzschirme Laitovo können an alle Seitenscheiben und an die Heckscheibe angebracht werden, dienen zum Schutz von Innenraum des Autos vor Staub, Feinabfall und Insekten.',
        'en' => '<b>Protective screens of Laitovo</b> are the most expensive products in price segment  Premium for the buyers who choose the best car accessories. Laitovo is the most profitable product among other reference positions.',
    ],
    'mediumCar' => [
        'de' => 'Mittelklassenwagen(Limousine,Crossover)',
        'en' => 'Medium car (Sedan, Crossover Utility Vehicle)',
    ],
    'FD' => [
        'de' => 'Vordere Seitenscheiben',
        'en' => 'Sun shades for front side doors',
    ],
    'RD' => [
        'de' => 'Hintere Seitenscheiben',
        'en' => 'Sun shades for rear side doors',
    ],
    'rearHemisphere' => [
        'de' => 'Set ab B-Säule',
        'en' => 'Rear hemisphere',
    ],
    'completeSet' => [
        'de' => 'Voller Set',
        'en' => 'Complete Set',
    ],
    'retailPrice' => [
        'de' => 'Einzelverkaufspreis',
        'en' => 'Retail',
    ],
    'salePrice' => [
        'de' => 'Grosshandelspreis',
        'en' => 'Wholesale',
    ],
    'withoutVents' => [
        'de' => 'Preise sind für Set  ohne Vents angesetzt',
        'en' => 'Prices are indicated without vents',
    ],
    'offRoad' => [
        'de' => 'Gelände- und Luxusklassewagen',
        'en' => 'Off-road  and Luxury vehicles',
    ],
    'chikoEconomy' => [
        'de' => 'Schutzschirme Standard <small>Economy</small>',
        'en' => 'CHIKO  <small>Economy</small>',
    ],
    'chikoDesc' => [
        'de' => '&nbsp;&nbsp; Ein Standart-Schutzschirm ist ein preiswertes Produkt von hoher Qualität für einen anspruchsvollen Kunden. </br>  &nbsp;&nbsp; Bei einem Standard Schutzschirm handelt es sich um ein festes Metallgehäuse, das mit grobmaschigem Gewebe beschlagen wird. Das Gewebe verhindert das Eindringe von schädlicher direkter Sonneneinstrahlung ins Auto und schützt damit Fahrer und Fahrgästen.</br>  &nbsp;&nbsp; Schutzschirme Standard können an alle Seitenscheiben und an die Heckscheibe angebracht werden, dienen zum Schutz von Innenraum des Autos vor Staub, Feinabfall und Insekten.',
        'en' => 'Protective screens of Chiko are not  expensive products of high quality for the demanding customers. Specific-customer pricing is appliable to the extent of this product',
    ],
    'favourableTerms' => [
        'de' => 'Günstige Bedingungen für Unternehmen jeder Grösse',
        'en' => 'Favourable terms for business  on international scale',
    ],
    'discount30' => [
        'de' => '30% Rabatt <small>für jeden Händler</small>',
        'en' => '30% Discount <small>for all dealers</small>',
    ],
    'discount30Desc' => [
        'de' => 'Jeder potentieller Händler kann Laitovo-Produkte ohne besondere Anforderungen mit 30% Rabatt bestellen. Der Gesamtbetrag für erste Bestellung muss nicht weniger als 250 €sein.',
        'en' => 'Every potential dealer can buy Laitovo wholesale  using discount in amount of 30 % without any special requirements. The first amount of an order  is supposed to be no  less than 250 euros. All the following orders have no limitations.',
    ],
    'discount40' => [
        'de' => '40% Rabatt',
        'en' => '40% Discount <small>for Laitovo brand building</small>',
    ],
    //Данное предложение включает дополнительные требования
    'discountReq' => [
        'de' => 'Dieses Angebot umfasst zwei folgende zusätzliche Forderungen',
        'en' => 'The provided offer  is made of the following supplementary  requirements',
    ],
    'discount40Desc1' => [
        'de' => 'Verfügbarkeit von Laitovo Produkten auf der Web Seite',
        'en' => 'Production is available on the web site of the Laitovo partner',
    ],
    'discount40Desc2' => [
        'de' => 'Webpromotion der Seite mit Laitovo-Produkte. Werbekampagnen werden mit Hersteller abgestimmt.',
        'en' => 'Search engine optimnization of the web site , negotiation of advertising companies with competent agencies',
    ],
    'discount50' => [
        'de' => '50% Rabatt',
        'en' => '50% Discount <small>for exclusive brand building of Laitovo</small>',
    ],
    'discount50Desc3' => [
        'de' => 'Nichtvorhandensein  der Angebote mit Schutzschirmen für Autofenster  von anderen Herstellern  im  Katalog',
        'en' => 'Absence of the quotations for protective screens  of car windows from other producers',
    ],
    'discount5000' => [
        'de' => '50% Rabatt ',
        'en' => '50% Discount for one-time purchase in amount of more than 5000 euro',
    ],
    'discount5000Desc' => [
        'de' => '<b>Abfassung und Bezahlung</b> von der einmaligen Bestellung im Wert nicht weniger als 5000 €.',
        'en' => '<b>Ordering and performing</b> payment in an amount of at least  5000 euro',
    ],
    'otherTerms' => [
        'de' => 'Sonstige Bedingungen',
        'en' => 'Other terms and condition',
    ],
    'shippingHamburg' => [
        'de' => 'Lieferung  ab Lager, Hamburg',
        'en' => 'Shipping ex factory warehouse, Hambur',
    ],
    'dropshipping' => [
        'de' => 'Dropshipping',
        'en' => 'Dropshipping',
    ],
    'permanentRenewal' => [
        'de' => 'Ständige Erneuerung vom Produktprogramm',
        'en' => 'Permanent renewal of product range',
    ],
    'technicalSupport' => [
        'de' => 'Betreuender Kundendienst',
        'en' => 'Technical support service',
    ],
    'individualContract ' => [
        'de' => 'Individuelle Vertragsbedingungen',
        'en' => 'Individual contract conditions',
    ],
    'notEuropeanUnion' => [
        'de' => 'Für die Geschäftspartner aus nicht-EU-Länder individuelle Vertragsbedingungen sind vorgesehen.',
        'en' => 'For the partners who are from the countries  which are not the members of the European Union. Individual contract conditions could be applied',
    ],
    'withLaitovo' => [
        'de' => 'Es ist leicht mit <b>Laitovo</b> zu verdienen!',
        'en' => 'It is easy to earn with <b>Laitovo</b>',
    ],
    'dealerDiscount' => [
        'de' => 'Händlerrabatt bis auf 50 %',
        'en' => 'Dealer discount up to 50 %',
    ],
    'partner' => [
        'de' => 'Partner der zum Dealer wurde kann den Rabbat im Betrag von 30% bis 50% bekommen',
        'en' => 'Partner which became Dealer , can get discount in  amount from 30% to 50%',
    ],
    'unlimitedOrders' => [
        'de' => 'Unlimitierte Bestellungen',
        'en' => 'Unlimited orders',
    ],
    'notMinimum' => [
        'de' => 'Wir bestimmen keine minimalen Beträge für die Grosshandelsbestellungen',
        'en' => 'We do not define minimum purchasing limits for wholesale orders',
    ],
    'shippingWarehouse' => [
        'de' => 'Lieferung an den Kunden  ab Lager vom Hersteller',
        'en' => 'Directly shipping of goods from warehouse in Hamburg to your buyers.',
    ],
    'specialistsAlways' => [
        'de' => 'Fachberater sind  immer bereit professionele Beratung zur Montage und Betrieb von den Schutzschirmen zu machen',
        'en' => 'Specialists are always ready to give professional consultation  on installation and operation of the sun shades of car windows',
    ],
    'marketingFolders' => [
        'de' => 'Werbematerialien',
        'en' => 'Marketing  folders',
    ],
    'downloadTemplates' => [
        'de' => 'Laden Sie bitte die Vorlagen  herunter und bestellen Sie Werbeartikel  zum Selbstkostenpreis in ihrem Benutzerkonto',
        'en' => 'Download the templates and order promotion materials at cost price in your user account',
    ],
    'wideModelRange' => [
        'de' => 'Breite Modelreihe',
        'en' => 'Wide model range',
    ],
    'permanentRenewalNew' => [
        'de' => 'Ständige Erneuerung vom Produktionskatalog  für die Neufahrzeuge',
        'en' => 'Permanent renewal of product range',
    ],
    'signUp' => [
        'de' => 'Melden Sie sich bei uns an',
        'en' => 'REQUEST FOR PARTNESHIP',
    ],
    'fillForm' => [
        'de' => 'Füllen Sie das vorliegende Anmeldeformular. Bewerben Sie sich zur Partnerschaft mit Laitovo',
        'en' => 'Fill out the form and get Dealer status of Laitovo',
    ],
    'name' => [
        'de' => 'Ihr Name',
        'en' => 'Your name',
    ],
    'telephone' => [
        'de' => 'Telefon',
        'en' => 'Telephone',
    ],
    'bewareImitations' => [
        'de' => 'Vorsicht <span>Fälschung</span>',
        'en' => 'Beware of <span>imitations</span>',
    ],
    'patented' => [
        'de' => 'Die ganze Produktion von Laitovo ist patent- und urheberrechtlich geschützt',
        'en' => 'All the products are patented and protected  by copyright law',
    ],
    'brandLaitovo' => [
        'de' => '<p>Handelsmarke <b>Laitovo</b> hat Anerkennung  in den Ländern von Europa und Asien gefunden. Wir tragen die volle Verantwortung  vor unseren Kunden weil jeder von ihnen zum Mitglied von Laitovo Familie wird. Oft  versuchen andere Hersteller  Handelsmarke Laitovo  zu missbrauchen  und treten im fremden Namen auf  um Geld  auf Ausschussproduktion zu verdienen.</p>',
        'en' => '<p>Brand <b>Laitovo</b> is recognized within countries of  both  Europe and Asia. We are liable before our customers as each of them becomes the part of Laitovo family at once. Other producers make  often attempts to make use of Brand Laitovo, deceive and earn with help of false identity selling defective goods.</p>',
    ],
    'inventor' => [
        'de' => '<p>Laitovo ist Verfahrenserfinder  der Schutzschirmherstellung  und Anbringungsart  für Autofenster.</p>',
        'en' => '<p>Laitovo is an experienced inventor of the production technique for sun shades of car windows  and manner of fastening.</p>',
    ],
    'soleRights' => [
        'de' => '<p><b>Laitovo</b> ist ein einziger Rechtsträger vom Verfahren der Schutzschirmherstellung  und Anbringungsart  für Autofenster.</p>',
        'en' => '<p><b>Laitovo</b> is a sole rights holder of the production technique for sun shades of car windows  and manner of fastening.</p>',
    ],
    'dealerLicense' => [
        'de' => '<p>Partner erhält ein Lizenzrecht <b>Laitovo</b> Produktion zu vertreiben und Absicherung  einen langfristigen Geschäftsverkehr zu entwickeln.</p>',
        'en' => '<p><b>Laitovo</b> dealer obtains a License for the right to distribute products and keep assurance in force to develop long-term business.</p>',
    ],
    'manufacturerWebsite' => [
        'de' => 'Zum Herstellersinternetseite',
        'en' => 'Visite manufacturer\'s websit',
    ],
    'socialNetworks' => [
        'de' => 'Wir in soziale Netzwerke',
        'en' => 'We in social networks',
    ],
    'licensedCompany' => [
        'de' => 'Alternative zu Scheibentönung, Schutzschirme für Autofenster. Alle Produkte sind  von unserer Gesellschaft entwickelt und patentgeschützt',
        'en' => '2014 © Laitovo
Alternative to vehicle tinting, protective screens for car windows are developed and licensed by our company',
    ],
];