<!-- Script Begins -->

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery-1.11.0.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->theme->baseUrl; ?>/../default/js/jquery-ui.min.js"></script>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bootstrap.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bootstrapValidator.min.js"></script>
<!-- Slider and Features Canvas -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.superslides.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.flexslider-min.js"></script>
<!-- Overlay -->
<!-- Screenshot -->
<!-- Portfolio -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.easing.1.3.js"></script>
<!-- Video -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.mb.YTPlayer.js"></script>
<!-- Counting Section -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/fancybox/fancybox.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.appear.js"></script>
<!-- Expertise Circular Progress Bar -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/inputmask.js?ver=1"></script>
<!-- Twitter -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/tweet/carousel.js"></script>
<!-- slider-->
<!-- Custom -->
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/custom.js?ver=10"></script>
<!-- Color -->
<!-- Script Ends -->
<script src="/themes/landing_1/js/notie.js"></script>
<script src="/themes/landing_1/js/animsition.min.js"></script>
<script src="/themes/landing_1/elevatezoom-master/jquery.elevatezoom.min.js"></script>

<script>
    $('.language span').click(function () {
        $(this).next().toggle();
    });
</script>

<script type="text/javascript">
    if ($(window).width() > 600) {
        $(".zoom_img").elevateZoom({cursor: "crosshair"});
        $(".zoom_img1").elevateZoom({cursor: "crosshair", zoomWindowPosition: 11});
    }
    $('.autocat .goods .block:nth-child(4n) .thumb img:nth-child(1)').elevateZoom({
        cursor: "crosshair",
        zoomWindowPosition: 11
    });
    $('.autocat .goods .block:nth-child(4n-1) .thumb img:nth-child(1)').elevateZoom({cursor: "crosshair"});
    $('.autocat .goods .block:nth-child(4n-2) .thumb img:nth-child(1)').elevateZoom({cursor: "crosshair"});
    $('.autocat .goods .block:nth-child(4n-3) .thumb img:nth-child(1)').elevateZoom({cursor: "crosshair"});


    $(document).ready(function () {
        var ordersitelaitovo = 0;
        $('.ordersitelaitovo').click(function () {
            ordersitelaitovo = 1;
            $('.mybyform_submitbtn').click();
            ordersitelaitovo = 0;
            return false;
        });

        $('.mybyform').submit(function () {
            redirect = false;
            if (ordersitelaitovo) redirect = true;
            $('.mybyform_submitbtn').hide();
            $('.ordersitelaitovo').hide();
            $('.mybyform_submitbtn').next().show();
            $.post('/', {
                'User[name]': $(this).find('.mybyform_name').val(),
                'User[phone]': $(this).find('.mybyform_phone').val(),
                'User[email]': $(this).find('.mybyform_email').val(),
                'Orders[comment][1]': 'Автомобиль:',
                'Orders[comment][2]': $(this).find('.mybyform_car').val(),
                'Orders[comment][3]': 'Комментарий:',
                'Orders[comment][4]': $(this).find('.mybyform_text').val() + ' ' + ($('#magnitplus').is(":checked") ? '+ магнитные держатели (400 руб)' : ''),
                'Orders[code]': '',
                'Orders[region]': 1,
                'nosendemail': redirect,
                'OrderOneClick': 1
            }, function (data) {
                if (data.status == 'ok') {
                    $('.btnclosemodal').click();
                    // yaCounter34540060.reachGoal("mgnt-zakaz");
                    if (redirect) {
                        if ($('.mybyform_cartochka').find('input').is('.basket_id')) {
                            komplekt = '';
                            if ($('.mybyform_cartochka').find('.basket_id').length > 1)
                                $.each($('.mybyform_cartochka').find('.basket_id'), function (index, value) {
                                    komplekt = komplekt + $(this).val() + ',';
                                });

                            document.location.href = 'https://laitovo.ru/catalog/cars/<?=@$_GET["link"]?>?orderinlandinghost=laitovo.org&fullversion=1&komplekt=' + komplekt + '&product=' + $('.mybyform_cartochka').find('.basket_id').val() + '&orderinlanding=' + data.order;
                        } else {
                            document.location.href = 'https://laitovo.ru/catalog/cars/<?=@$_GET["link"]?>?orderinlandinghost=laitovo.org&fullversion=1&orderinlanding=' + data.order;
                        }
                    } else {
                        notie.alert(1, 'Ваша заявка отправлена!', 4);
                    }

                    $('.mybyform_name').val('');
                    $('.mybyform_phone').val('');
                    $('.mybyform_email').val('');
                    $('.mybyform_car').val('');
                    $('.mybyform_text').val('');
                } else {
                    $.each(data.response, function (key, value) {
                        notie.alert(3, value, 4);
                    })
                }
                $('.mybyform_submitbtn').show();
                $('.ordersitelaitovo').show();
                $('.mybyform_submitbtn').next().hide();

                return false;
            }, 'json');
            return false;
        });


        $('.orderscrolbtn2').click(function () {
            $('.mybyform_text').val($(this).next().val());
            $('.mybyform_cartochka .thumbnail').html('<div class="thumb">' + $(this).parent().html() + '</div>');
            $('.link_open_order').click();
            return false;
        });
        $('.orderscrolbtn').click(function () {
            $('.mybyform_car').val($('.hidden_car_name').val());
            $('.mybyform_text').val($(this).parent().parent().find('.nameprod').text() + ' ' + $(this).parent().parent().find('.priceprod').text());
            $('.mybyform_cartochka .thumbnail').html($(this).parent().parent().parent().html());
            $('.link_open_order').click();
            return false;
        });
        $(".fancybox").fancybox({
            padding: 1
        });
    });


    $(function () {


        function second_passed() {


            $('#video_bg_slider').html('' +
                '<div class="flexslider">' +
                '<ul class="slides">' +
                '<li>' +
                '<iframe width="540" height="305" src="//www.youtube.com/embed/23ZVuUdhByw?feature=player_embedded&amp;rel=0&amp;autoplay=0" frameborder="0" allowfullscreen></iframe>' +
                '</li>' +
                '<li>' +
                '<iframe width="540" height="305" src="//www.youtube.com/embed/0NX78rVyJnc?feature=player_embedded&amp;rel=0&amp;autoplay=0" frameborder="0" allowfullscreen></iframe>' +
                '</li>' +
                '<li>' +
                '<iframe width="540" height="305" src="//www.youtube.com/embed/qNbeL8IuC28?feature=player_embedded&amp;rel=0&amp;autoplay=0" frameborder="0" allowfullscreen></iframe>' +
                '</li>' +
                '<li>' +
                '<iframe width="540" height="305" src="//www.youtube.com/embed/xX8qJFlC4g8?feature=player_embedded&amp;rel=0&amp;autoplay=0" frameborder="0" allowfullscreen></iframe>' +
                '</li>' +
                '<li>' +
                '<iframe width="540" height="305" src="//www.youtube.com/embed/IU-i-o9j0QQ?feature=player_embedded&amp;rel=0&amp;autoplay=0" frameborder="0" allowfullscreen></iframe>' +
                '</li>' +
                '<li>' +
                '<iframe width="540" height="305" src="//www.youtube.com/embed/yoQebBHOmMU?feature=player_embedded&amp;rel=0&amp;autoplay=0" frameborder="0" allowfullscreen></iframe>' +
                '</li>' +
                '<li>' +
                '<iframe width="540" height="305" src="//www.youtube.com/embed/EBzdSu2fQ9Q?feature=player_embedded&amp;rel=0&amp;autoplay=0" frameborder="0" allowfullscreen></iframe>' +
                '</li>' +
                '</ul>' +
                '</div>' +
                '');

            $('.flexslider').flexslider({

                animation: 'fade',

                slideshow: false,

                animationLoop: false,

                controlNav: true,
                directionNav: true

            });
        }

        $('.loaddoopvideo').click(function () {
            if ($('#video_bg_slider').html() == '') {
                $('.loaddoopvideo').remove();
                $('#video_bg_slider').removeClass('hidden');
                second_passed();
            }
        })


    })

    $(function () {

        $('.440044clickbtn').click(function () {
            // yaCounter27506457.reachGoal("440044click");
        });

        $('.cooperationclickbtn').click(function () {
            // yaCounter27506457.reachGoal("cooperationclick");
        });


    });


    $(function () {



        /* --------------------------------------------

        Fixed Menu on Scroll

        -------------------------------------------- */

        // $("#sticky-section").sticky({topSpacing:0});


        /* --------------------------------------------

        Screenshot Scripts

        -------------------------------------------- */

        'use strict';
        jQuery("#screenshot").owlCarousel({
            items: 5,
            lazyLoad: true,
            autoPlay: false,
            navigation: false,
            itemsCustom: false,
            itemsDesktop: [1199, 4],
            itemsDesktopSmall: [991, 3],
            itemsTablet: [768, 2],
            itemsTabletSmall: false,
            itemsMobile: [479, 1]
        });


        /* --------------------------------------------

        Contact Form

        -------------------------------------------- */


        'use strict';

        $('#contactform').bootstrapValidator({

            message: '',

            feedbackIcons: {

                valid: 'glyphicon glyphicon-ok',

                invalid: 'glyphicon glyphicon-remove',

                validating: 'glyphicon glyphicon-refresh'

            },

            fields: {

                contact_name: {

                    validators: {

                        notEmpty: {

                            message: ''

                        }

                    }

                },

                contact_email: {

                    validators: {

                        notEmpty: {

                            message: ''

                        },

                        emailAddress: {

                            message: ''

                        }

                    }

                },

                contact_phone: {

                    validators: {

                        notEmpty: {

                            message: ''

                        }

                    }

                },

                contact_time: {

                    validators: {

                        notEmpty: {

                            message: ''

                        }

                    }

                },

                contact_message: {

                    validators: {

                        notEmpty: {

                            message: ''

                        }

                    }

                }

            },

            submitHandler: function (validator, form, submitButton) {

                $('.contact-form').addClass('ajax-loader');

                var actionForm = $('#contactform');

                $.post('<?= Yii::app()->createUrl('api/landing/dealer') ?>', {
                    'User[name]': $('#contact_name').val(),
                    'User[email]': $('#contact_email').val(),
                    'User[phone]': $('#contact_phone').val(),
                    'User[region]' : $('#region').val(),
                    'cooperation' : 1
                }, function (data) {
                    if (data.status == 'ok') {
                        alert("Ваша заявка отправлена! Спасибо!");
                        //document.location.href = 'https://laitovo.ru';
                    } else
                        alert(data.response);
                    submitButton.removeAttr("disabled");
                    $('.contact-form').removeClass('ajax-loader');
                    resetForm($('#contactform'));
                }, 'json');

                return false;
            },

        });


        function resetForm($form) {

            $form.find('input:text, input:password, input, input:file, select, textarea').val('');

            $form.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');

        }


    })


</script>

<script type="text/javascript">
    $('.feedbackcall').click(function () {
        $.get("<?=Yii::app()->createUrl('ajax/call/feedbackcall?phone=')?>" + $('.feedbackphone').val());
        /*yaCounter<//?=$metrika?>.reachGoal('zak-zvonok');*/
        $('.close').click();
    })
</script>

<? $this->widget('chat.components.LiveChat', ['render' => 'org']); ?>

<script type="text/javascript">

    $(".formcircle[type='text']").autocomplete({
        source: "<?=Yii::app()->createUrl('ajax/search/index')?>",
        select: function (event, ui) {
            $(".formcircle[type='text']").val(ui.item.value);
            $('.formcircleform').submit();
            return false;
        }
    });
    $('.orderscrolbtn').click(function () {
        // $('#contact_comment2').val($(this).parent().parent().find('.nameprod').text()+' '+$(this).parent().parent().find('.priceprod').text());
        // $('#contactform').submit();
    });

    <?if (!(@$_GET['link'] || @$_GET['searchid'])):?>
    function second_passed() {
        $('.panel-heading.openchat').click()
    }

    function second_passed1() {
        $('.livechatblock').hide('slow');
        $('.panel-heading .closechat').click()
        $('.livechatblock').show('fast');
    }

    if ($('html, body').width() > 500 && $('.livechatblock').hasClass('closed')) {
        setTimeout(second_passed, 5000);
        setTimeout(second_passed1, 7000);
    }
    <?endif?>

    $(function () {

        var
            $body = $('body'),
            videoEle,
            $videoBgSelector = $('#bg-video'),
            videoBgEle = $videoBgSelector.get(0),
            $header = $('.header'),
            $modalVideo = $('.modal-video'),
            $fadeControls = $('.modal-video .fade-control'),
            $launchButton = $('.btn-launch-video'),
            $hasHint = $('.has-hint'),
            $hero = $('.level-hero'),
            controlsTimer = null,
            eventLastX = 0,
            eventLastY = 0,
            desktopView = Modernizr.mq("screen and ( min-width: 1200px )"),
            canPlaceholder = Modernizr.input.placeholder;
        baseUrl = window.location.hostname;

        if (Modernizr.video && desktopView && videoBgEle) {
            videoBgEle.load();
            videoBgEle.play();

            if (typeof videoBgEle.loop == 'boolean') {
                // loop supported
                videoBgEle.loop = true;
            }
            else {
                // loop property not supported
                videoBgEle.on('ended', function () {
                        this.currentTime = 0;
                        this.play();
                    },
                    false);
            }

        }

        else {
            $videoBgSelector.remove();
        }

    });

    $(document).ready(function () {
        $(".animsition").animsition({
            inClass: 'fade-in',
            outClass: 'fade-out',
            inDuration: 1500,
            outDuration: 800,
            linkElement: '.animsition-link',
            // e.g. linkElement: 'a:not([target="_blank"]):not([href^=#])'
            loading: true,
            loadingParentElement: 'body', //animsition wrapper element
            loadingClass: 'animsition-loading',
            loadingInner: '', // e.g '<img src="loading.svg" />'
            timeout: false,
            timeoutCountdown: 5000,
            onLoadEvent: true,
            browser: ['animation-duration', '-webkit-animation-duration'],
            // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
            // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
            overlay: false,
            overlayClass: 'animsition-overlay-slide',
            overlayParentElement: 'body',
            transition: function (url) {
                window.location.href = url;
            }
        });
    });

</script>


<?php echo $this->PageContent; ?>

<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/select2.min.js"></script>
<script>
    $(".select-search.selectregions").select2({
        allowClear : true,
        placeholder: 'Select country'
    });
</script>