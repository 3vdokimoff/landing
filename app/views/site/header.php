<div id="video-slide" class="home-slider relative">
    <!-- Background Slider Begins -->
    <div id="slides" class="home-slider relative">
        <div style="width:100%;height:100%;opacity:0.3;background:#000;text-align:center;vertical-align-middle;position: absolute;z-index:2;">
        </div>
        <!-- Video Section-->
        <video muted loop id="bg-video" class="fullscreen-video" style="width:100%;">
            <source src="video_new_.webm" type="video/webm">
            <source src="video_new_.mp4" type="video/mp4">
        </video>
        <!-- End Video Section-->
        <div id="sticky-section-sticky-wrapper" class="sticky-wrapper is-sticky" style="height: 0px;">
            <!-- Top Section Begins -->
            <div id="sticky-section-no" class="sticky-navigation" style="z-index:4;">
                <div id="top-section" class="top-container absolute container">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Logo Begins -->
                            <div id="logo" class="site-logo col-md-1 no-padding">
                                <a title="Logo" href="">
                                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logowhite.png"></a>
                                <p>
                                    <?= $dealer_lang['sale'][$lang] ?>
                                </p>
                            </div>
                            <!-- Logo Ends -->
                            <!-- Navigation Menu Begins -->
                            <div id="navigation" class="col-md-11 no-padding navbar top-navbar">
                                <!-- Mobile Nav Toggle Begins -->
                                <div class="navbar-header nav-respons">
                                    <button data-target=".bs-navbar-collapse" data-toggle="collapse"
                                            type="button" class="navbar-toggle">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <!-- <a href="tel:88005550120"
                                       style="color:#fff;margin-right:10px;padding-bottom: 7px;"
                                       class="navbar-toggle">
                                        <i class="fa fa-phone"></i>
                                    </a> -->
                                   <!--  <a href="skype:laitovo_company?chat"
                                       style="color:#fff;margin-right:10px;padding-bottom: 7px;"
                                       class=" navbar-toggle">
                                        <i class="fa fa-skype"></i>
                                    </a> -->


                                </div>
                                <!-- Mobile Nav Toggle Ends -->
                                <!-- Menu Begins -->

                                <nav id="topnav" role="navigation"
                                     class="collapse navbar-collapse bs-navbar-collapse no-padding">
                                    <ul class="nav navbar-nav navbar-right uppercase">
                                        <li><a href="#products" class="scroll"><?= $dealer_lang['prodPrice'][$lang] ?></a></li>
                                        <!-- <li><a href="#features" class="scroll">Преимущества</a></li> -->
                                        <li><a href="#partner"
                                               class="scroll"><?= $dealer_lang['terms'][$lang] ?></a></li>
                                        <li><a href="#zayavka" class="scroll"><?= $dealer_lang['getDealerStatus'][$lang] ?></a></li>
                                        <li><a href="#contactsection" class="scroll"><?= $dealer_lang['contacts'][$lang] ?></a></li>
                                            <li><b >+49 (0)40-87407233</b>
                                            <a href="#" class="" data-target=".bs-example-modal-sm" data-toggle="modal" ><small><?= $dealer_lang['callBack'][$lang] ?></small></a></li>
                                         <li>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </li>
                                        <li>
                                            <ul class="nav navbar-nav uppercase">
                                                <li class="language"><span><img style="width: 25px;" src="/themes/landing_1/images/<?= $lang ?>.png"></span>
                                                    <ul class="submenu" style="margin: 0; padding: 0; list-style: none;">
                                                        <li>
                                                            <a href="/?lang=<?= $lang_search ?>"><img  style="width: 25px;"
                                                                        src="/themes/landing_1/images/<?= $lang_search ?>.png"></a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li><li>
                                            <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-sm1">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title" id="mySmallModalLabel"><?= $dealer_lang['callBack'][$lang] ?></h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <iframe scrolling="no" frameborder="0" allowtransparency="true" style="height:90px; width:400px;" src="https://callback.inopla.de/callback/window.html?bk=f0e1716e52cc7831246a6e1c1c522977"></iframe>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </li>

                                    </ul>
                                </nav>
                                <!-- Menu Ends -->
                            </div>
                            <!-- Navigation Menu Ends -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Top Section Ends -->
        <!-- Slide Text Begins -->
        <div class="text-slider-section absolute container">
            <div class="text-slider h-phone">
                <ul class="slide-text slides">
                    <li><?= $dealer_lang['slogan1'][$lang] ?></li>
                    <li><?= $dealer_lang['slogan2'][$lang] ?></li>
                    <li><?= $dealer_lang['slogan3'][$lang] ?></li>
                    <li><?= $dealer_lang['slogan4'][$lang] ?></li>
                </ul>
            </div>
            <!-- Fixed Text -->
            <h1 class="slide-fixed-text h-phone"><?= $dealer_lang['slogan5'][$lang] ?></h1>
            <!-- <img src="https://laitovo.eu/upload/images/Banner/3c22b341b9e440e80e5ce0e14eecf97e.png"
                 class="h-computer hidden-tablet"> -->
            <a href="#zayavka" class="scroll btn slide-btn btn-darkblue"><?= $dealer_lang['Zayavkaobslujivanie'][$lang] ?><i
                        class="fa  fa-briefcase"></i></a>
        </div>
        <!-- Background Slider Ends -->
    </div>
</div>