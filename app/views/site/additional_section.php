<section class="additional-section">
    <div class="container padding:100px">
        <!-- Title & Desc Row Begins -->
        <div class="row">
            <div class="col-md-12 header text-center">
                <!-- Title -->
                <div class="title">
                    <h2 class="white"> <?= $dealer_lang['withLaitovo'][$lang] ?>! </h2>
                </div>
                <div class="row ">
                    <br><br>
                    <div class="col-md-4">
                        <div class="additional-features animated" data-animation="fadeInLeft"
                             data-animation-delay="300">
                            <!-- Icon -->
                            <i class="fa fa-cart-arrow-down"></i>
                            <!-- Content -->
                            <div class="additional-content">
                                <h5><?= $dealer_lang['dealerDiscount'][$lang] ?></h5>
                            </div><p><?= $dealer_lang['partner'][$lang] ?></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="additional-features animated" data-animation="fadeInUp"
                             data-animation-delay="200">
                            <!-- Icon -->
                            <i class="fa fa-signal"></i>
                            <!-- Content -->
                            <div class="additional-content">
                                <h5><?= $dealer_lang['unlimitedOrders'][$lang] ?></h5>
                            </div>
                            <p><?= $dealer_lang['notMinimum'][$lang] ?></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="additional-features animated" data-animation="fadeInRight"
                             data-animation-delay="300">
                            <!-- Icon -->
                            <i class="fa fa-file-image-o"></i>
                            <!-- Content -->
                            <div class="additional-content">
                                <h5><?= $dealer_lang['marketingFolders'][$lang] ?></h5>
                            </div>
                            <p><?= $dealer_lang['downloadTemplates'][$lang] ?></p>
                        </div>
                    </div>
                </div>

                <div class="row ">
                    <div class="col-md-4 right-border">
                        <div class="additional-features animated" data-animation="fadeInRight"
                             data-animation-delay="400">
                            <!-- Icon -->
                            <p> &nbsp</p>
                            <i class="fa fa-truck"></i>
                            <!-- Content -->
                            <div class="additional-content">
                                <h5>Dropshipping </h5>
                            </div>
                            <p><?= $dealer_lang['shippingWarehouse'][$lang] ?></p>
                        </div>
                    </div>

                    <div class="col-md-4 ">
                        <div class="additional-features animated" data-animation="fadeInUp"
                             data-animation-delay="200">
                             <p> &nbsp</p>
                            <!-- Icon -->
                            <i class="fa fa-cogs"></i>
                            <!-- Content -->
                            <div class="additional-content">
                                <h5><?= $dealer_lang['technicalSupport'][$lang] ?></h5>
                            </div>
                            <p><?= $dealer_lang['specialistsAlways'][$lang] ?></p>
                        </div>
                    </div>
                    <div class="col-md-4 right-border">
                        <div class="additional-features animated" data-animation="fadeInRight"
                             data-animation-delay="400">
                             <p> &nbsp</p>
                            <!-- Icon -->
                            <i class="fa fa-car"></i>
                            <!-- Content -->
                            <div class="additional-content">
                                <h5><?= $dealer_lang['wideModelRange'][$lang] ?></h5>
                            </div>
                            <p><?= $dealer_lang['permanentRenewalNew'][$lang] ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="triangle-right-light hgray-shape" style="border-bottom: 70px solid #fff;"></div>
            <div class="triangle-left-light hgray-shape" style="border-bottom: 70px solid #fff;"></div>
</section>