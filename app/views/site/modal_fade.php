<!-- Copyright Section Ends -->
<style type="text/css">
    .mybyform_cartochka {
        text-align: center;
    }

    .mybyform_cartochka .btn {
        display: none;
    }

    .mybyform_cartochka img + img {
        display: none;
    }

    .modal-content {
        color: #333;
    }
</style>
<a href="#tinting_system_header" class="link_open_order" data-toggle="modal" data-target="#myModal"></a>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     style="z-index:9999;margin-top:60px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body ">
                <form class="mybyform">
                    <div class="col-sm-4 mybyform_cartochka">
                        <div class="thumbnail thumbnail- carsblock text-center">
                            <div class="thumb">
                                <img alt="Шторки Лайтово для Acura MDX"
                                     src="https://laitovo.eu/upload/images/Photo/thumbnail/79a82aa2438c48dff945115e42feaa5a_1.jpg">
                                <img src="https://laitovo.eu/upload/images/Banner/16746a6eedb3748764ff04a09912af2a_1.png"
                                     class="bespdoststiker"
                                     style="width: 50%;position: absolute;margin: -25% -25px;">
                                <div class="thumb-options light h-phone hidden-tablet">
                                    <input type="hidden" class="basket_id" value="28222">
                                </div>
                            </div>
                            <div class="caption text-center">

                                <a title="" class="caption-title"><s class="oldprice text-danger ">4700</s> <span
                                        class="priceprod">3995 руб.</span>
                                </a>
                                <br>
                                <span class="nameprod">Комплект защитных экранов Laitovo (ПБ, Medium / 10-20%)</span>
                            </div>
                        </div>
                        <label><img src="https://laitovo.eu/upload/images/Product/thumbnail/c899bf9a047a96e17cf077c1619b78be.jpg"
                                    width="40" style=""><br> <input type="checkbox" id="magnitplus"> + магнитные
                            держатели (400 руб)</label>

                    </div>
                    <div class="col-sm-8 row">
                        <div class="form-group col-xs-12">
                            <label>Ваше имя:</label>
                            <input required type="text" class="mybyform_name form-control">
                        </div>
                        <div class="form-group col-xs-12">
                            <label>Ваш e-mail:</label>
                            <input required type="email" class="mybyform_email form-control">
                        </div>
                        <div class="form-group col-xs-12">
                            <label>Ваш телефон:</label>
                            <input required type="tel" class="mybyform_phone form-control">
                        </div>
                    </div>
                    <div class="form-group col-xs-12">
                        <label>Автомобиль:</label>
                        <input type="text" class="mybyform_car form-control">
                    </div>
                    <div class="form-group col-xs-12">
                        <label>Комментарий:</label>
                        <textarea rows="5" class="mybyform_text form-control"></textarea>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <div class="col-xs-6 text-center">
                                <button type="submit" class="btn-block btn btn-danger mybyform_submitbtn">
                                    Отправить
                                </button>
                                <i class="fa fa-spinner fa-pulse" style="display:none;font-size:40px;"></i>
                            </div>
                            <div class="col-xs-6">
                                <button type="button" class="btn-block btn btn-default btnclosemodal"
                                        data-dismiss="modal">Отмена
                                </button>
                            </div>
                            <div class="col-xs-12 hidden">
                                <p>&nbsp;</p>
                                <a href="#" class="btn-info btn btn-block ordersitelaitovo">Оформить со скидкой 5% в
                                    интернет-магазине</a>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>