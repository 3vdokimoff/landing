<section style="background:#fff;">
    <div class="container text-center">
        <!--<div class="title">
            <h2 class=""> Другая продукция <span>Laitovo</span></h2>
        </div>-->
        <div style="padding-top: 5px;" class="row contact">
            <a class="btn btn-default 440044clickbtn" href="https://laitovo.ru" rel="nofollow"
               target="_blank"><?= $dealer_lang['manufacturerWebsite'][$lang] ?></a>
        </div>

    </div>
</section>

<!-- Download Now Section Begins -->
<!-- Copyright Section Begins -->
<section id="copyright" class="copyright">
    <div class="container">
        <!-- Social Media -->
        <div class="row social-media animated" data-animation="fadeInUp" data-animation-delay="400">
            <div class="col-md-12 text-center">
                <p><?= $dealer_lang['socialNetworks'][$lang] ?></p>
                <!-- Icons -->
                    <a href="https://www.facebook.com/pages/Laitovo-Germany/662610640484654" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="http://instagram.com/laitovo_europe/?ref=badge" target="_blank"><i class="fa fa-instagram"></i></a>
                    <a href="http://www.youtube.com/channel/UC2HTq2_aQBdx36qVLb-JBCw" target="_blank"><i class="fa fa-youtube"></i></a>
            </div>
        </div>
        <div class="row">
            <!-- Copyright Title -->
            <div class="col-md-12 text-center">

                <p><?= $dealer_lang['licensedCompany'][$lang] ?></p>

            </div>
        </div>
    </div>
</section>