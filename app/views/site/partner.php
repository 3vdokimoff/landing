<section id="partner">
    <div class="row">
        <div class="col-md-12 header text-center">
            <!-- Title -->
            <div class="title">
                <br>
                <h2><?= $dealer_lang['favourableTerms'][$lang] ?></h2>
            </div>
            <div class="col-md-8 col-md-offset-3 vde18">
                <div class="text-left">
                    <p>&nbsp;</p>
                </div>
            </div>
        </div>
         <p>&nbsp;</p>
    </div>
    <!-- Вставки романа -->
    <div class=" how-it-works-inner black">
        <div class="row">

            <div class="col-md-7">
                <div class="text-right animated" data-animation="fadeInLeft" data-animation-delay="500">
                    <h3 class="right"><?= $dealer_lang['discount30'][$lang] ?></h3>
                </div>
            </div>
        </div>
    </div>
    <div class="container how-it-works-inner black animated" data-animation="fadeInRight" data-animation-delay="500">
        <div class="row">
            <div class="col-md-7">
                <div class="text-left">
                    <!-- Desc -->
                    <p><?= $dealer_lang['discount30Desc'][$lang] ?></p>
                    <p>&nbsp;</p>
                    <!-- Lists -->
                </div>
            </div>
        </div>
    </div>
    <div class=" how-it-works-inner black">
        <div class="row">
            <div class="col-md-7 col-md-offset-5">
                <div class="text-left animated" data-animation="fadeInRight" data-animation-delay="500">
                    <h3 class="left"><?= $dealer_lang['discount40'][$lang] ?></h3>
                </div>
            </div>
        </div>
    </div>
    <div class="container how-it-works-inner black animated" data-animation="fadeInLeft" data-animation-delay="500">
        <div class="row">
            <div class="col-md-7 col-md-offset-5">
                <div class="text-left">
                    <!-- Desc -->
                    <p>
                        <?= $dealer_lang['discountReq'][$lang] ?>:
                    </p>
                    <ol>
                        <li><?= $dealer_lang['discount40Desc1'][$lang] ?></li>
                        <li><?= $dealer_lang['discount40Desc2'][$lang] ?></li>
                    </ol>
                    <p>&nbsp;</p>
                    <!-- Lists -->
                </div>
            </div>

        </div>
    </div>


    <div class=" how-it-works-inner black">

        <div class="row">

            <div class="col-md-7">
                <div class="text-left animated" data-animation="fadeInLeft" data-animation-delay="500">
                    <h3 class="right"><?= $dealer_lang['discount50'][$lang] ?></h3>
                </div>
            </div>


        </div>
    </div>

    <div class="container how-it-works-inner black animated" data-animation="fadeInRight" data-animation-delay="500">

        <div class="row">

            <div class="col-md-7">
                <div class="text-left">
                    <!-- Desc -->

                    <p>
                        <?= $dealer_lang['discountReq'][$lang] ?>:
                    </p>
                    <ol>
                        <li><?= $dealer_lang['discount40Desc1'][$lang] ?></li>
                        <li><?= $dealer_lang['discount40Desc2'][$lang] ?></li>
                        <li><?= $dealer_lang['discount50Desc3'][$lang] ?></li>
                    </ol>

                    <p>&nbsp;</p>


                    <!-- Lists -->
                </div>
            </div>


        </div>
    </div>


    <div class=" how-it-works-inner black">

        <div class="row">

            <div class="col-md-7 col-md-offset-5">
                <div class="text-left animated" data-animation="fadeInRight" data-animation-delay="500">
                    <h3 class="left"><?= $dealer_lang['discount5000'][$lang] ?></h3>
                </div>
            </div>


        </div>
    </div>

    <div class="container how-it-works-inner black animated" data-animation="fadeInLeft" data-animation-delay="500">

        <div class="row">

            <div class="col-md-7 col-md-offset-5">
                <div class="text-left">
                    <!-- Desc -->

                    <p><?= $dealer_lang['discount5000Desc'][$lang] ?></p>
                    <p>&nbsp;</p>


                    <!-- Lists -->
                </div>
            </div>


        </div>
    </div>


 <!--   <div class=" how-it-works-inner black">

        <div class="row">

            <div class="col-md-7">
                <div class="text-left animated" data-animation="fadeInLeft" data-animation-delay="500">
                    <h3 class="right"><?= $dealer_lang['otherTerms'][$lang] ?></h3> 
                </div>
            </div>


        </div>
    </div>

    <div class="container how-it-works-inner black animated" data-animation="fadeInRight" data-animation-delay="500">

        <div class="row">

            <div class="col-md-7">
                <div class="text-left">
                    <ol>
                        <li><?= $dealer_lang['shippingHamburg'][$lang] ?></li>
                        <li><?= $dealer_lang['dropshipping'][$lang] ?></li>
                        <li><?= $dealer_lang['permanentRenewal'][$lang] ?></li>
                        <li><?= $dealer_lang['technicalSupport'][$lang] ?></li>
                    </ol>

                    <p>&nbsp;</p>


                   
                </div>
            </div>

        </div>
    </div>
-->
    <div class=" how-it-works-inner black">

        <div class="row">

            <div class="col-md-7 ">
                <div class="text-left animated" data-animation="fadeInLeft" data-animation-delay="500">
                    <h3 class="right"><?= $dealer_lang['individualContract '][$lang] ?></h3>
                </div>
            </div>


        </div>
    </div>

    <div class="container how-it-works-inner black animated" data-animation="fadeInLeft" data-animation-delay="500">

        <div class="row">

            <div class="col-md-7 ">
                <div class="text-left">
                    <!-- Desc -->

                    <p><?= $dealer_lang['notEuropeanUnion'][$lang] ?></p>
                    <p>&nbsp;</p>


                    <!-- Lists -->
                </div>
            </div>


        </div>
    </div>

</section>