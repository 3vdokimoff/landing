<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Title and Meta Tags Begins -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<meta charset="utf-8">
		<!--[if IE]>
		<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
		<![endif]-->

		<meta name="description" content="<?php echo CHtml::encode($this->pageDescription); ?>" />
		<meta name="keywords" content="<?php echo CHtml::encode($this->pageKeywords); ?>" />
	    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
		<link rel="icon" href="/favicon.ico" type="image/x-icon">
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">

		<!-- Title and Meta Tags Ends -->
		<!-- Google Font Begins -->	
		<link href='//fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700italic,700,900,900italic' rel='stylesheet' type='text/css'>
		<link href='//fonts.googleapis.com/css?family=Raleway:400,700,600,500,300,200,100,800,900' rel='stylesheet' type='text/css'>
		<!-- Google Font Ends -->		
		<!-- CSS Begins-->	
		<link href='//netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css' rel='stylesheet' type='text/css'/>
		<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/flaticon.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/portfolio.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/animate.min.css" rel="stylesheet" type="text/css"/>
		<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/prettyPhoto.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/flexslider.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/YTPlayer.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/tweet-carousel.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/slider.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/cool-slider.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo Yii::app()->theme->baseUrl; ?>/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css">
		<!-- Main Style -->
		<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css?ver=5" rel="stylesheet" type="text/css" />
		<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/responsive.css" rel="stylesheet" type="text/css" />
		<!-- Color Panel -->
		<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/color_panel.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/animsition.min.css" rel="stylesheet" type="text/css" />
		<!-- Skin Colors -->
		<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/color-schemes/asphalt.css" id="changeable-colors" rel="stylesheet" type="text/css" />
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/select2.min.css" rel="stylesheet" type="text/css" />
		<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/modernizr.js"></script>
	</head>
	<body class="dark-version animsition">
		<?=$content?>
	</body>
</html>


